module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    autoprefixer: {
            options: {
              browsers: ['last 2 versions', '> 1% in PL', 'ie 8'] 
            },
            dist: {
               src: 'css/style.css',
               dest: 'css/styleprefixed.css'
            }
          },
    watch:{
        autoprefixer:
        {
           files: "css/style.css",
           tasks: ['autoprefixer']
        }
    },
    cssmin: {
        options: {
             mergeIntoShorthands: false,
              roundingPrecision: -1
        },
        target: {
             files: {
            'css/style.min.css': ['css/style.css']
          }
        }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  // Default task(s).
  grunt.registerTask('default', ['autoprefixer']);

};